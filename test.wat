(module

  (func (export "bswap32_shift") (param i32) (result i32)
    local.get 0
    i32.const 24
    i32.shl
    local.get 0
    i32.const 65280
    i32.and
    i32.const 8
    i32.shl
    i32.or
    local.get 0
    i32.const 8
    i32.shr_u
    i32.const 65280
    i32.and
    local.get 0
    i32.const 24
    i32.shr_u
    i32.or
    i32.or
  )

  (func (export "bswap32_rot") (param i32) (result i32)
    (i32.or
    (local.get 0)
    (i32.const 0x00FF00FF)
    (i32.and)
    (i32.rotr (i32.const 8))

    (local.get 0)
    (i32.const 0xFF00FF00)
    (i32.and)
    (i32.rotl (i32.const 8))))

  (func (export "bswap32_shift_loop") (param i32) (result i32)
        (local i32)
        (local i32)
        i32.const       0
        local.set       1
        i32.const       500000000 ;; Loop counter
        local.set       2
        loop                                            
          
    ;;;;;;;;;; PUT TEST FUNCTION HERE ;;;;;;;;;;;;

    local.get 0
    i32.const 24
    i32.shl
    local.get 0
    i32.const 65280
    i32.and
    i32.const 8
    i32.shl
    i32.or
    local.get 0
    i32.const 8
    i32.shr_u
    i32.const 65280
    i32.and
    local.get 0
    i32.const 24
    i32.shr_u
    i32.or
    i32.or

    ;;;;;;;;;; PUT TEST FUNCTION HERE ;;;;;;;;;;;;

          ;; increment argument
          local.get       0
          i32.const       0x01020301
          i32.add
          local.set       0

          ;; loop stuff
          local.get       1
          i32.xor 
          local.set       1
          local.get       2
          i32.const       -1
          i32.add 
          local.tee       2
          br_if           0
        end
        local.get       1

  )


    (func (export "bswap32_rot_loop") (param i32) (result i32)
        (local i32)
        (local i32)
        i32.const       0
        local.set       1
        i32.const       500000000 ;; Loop counter
        local.set       2
        loop                                            
          
    ;;;;;;;;;; PUT TEST FUNCTION HERE ;;;;;;;;;;;;

    (i32.or
      (local.get 0)
      (i32.const 0x00FF00FF)
      (i32.and)
      (i32.rotr (i32.const 8))

      (local.get 0)
      (i32.const 0xFF00FF00)
      (i32.and)
      (i32.rotl (i32.const 8)))


    ;;;;;;;;;; PUT TEST FUNCTION HERE ;;;;;;;;;;;;

          ;; increment argument
          local.get       0
          i32.const       0x01020301
          i32.add
          local.set       0

          ;; loop stuff
          local.get       1
          i32.xor 
          local.set       1
          local.get       2
          i32.const       -1
          i32.add 
          local.tee       2
          br_if           0
        end
        local.get       1

  )
)